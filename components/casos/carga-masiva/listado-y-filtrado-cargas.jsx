import React from 'react';
import {Controller, useForm} from "react-hook-form";
import {Card} from "primereact/card";
import {Button} from "primereact/button";
import {Dropdown} from "primereact/dropdown";
import {classNames} from "primereact/utils";
import {Calendar} from "primereact/calendar";
import {InputText} from "primereact/inputtext";
import ListadoCargas from "@/components/casos/carga-masiva/seccion-tabla-listado/listado-cargas";

const ListadoYFiltradoCargas = () => {
    return (<>
        <Card>
           <ListadoCargas/>
        </Card>
    </>);
};

export default ListadoYFiltradoCargas;
