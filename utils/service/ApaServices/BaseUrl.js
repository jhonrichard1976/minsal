import axios from "axios";


export const APACodigoFonasaAPI = axios.create({
    baseURL: 'http://10.69.206.32:8080/api-apa-codigo-fonasa/api'
})

export const BiopsiaAPI = axios.create({
    baseURL: 'http://10.69.206.32:8080/api-apa-biopsia/api'

})

export const CoreCodigoFonasaAPI = axios.create({
    baseURL: 'http://localhost:8045/convenios'
})

export const CoreProfesionalesAPI = axios.create({
    baseURL: 'http://localhost:8046/coreProfesionales'
})

export const CoreServiciosAPI = axios.create({
    baseURL: 'http://10.69.206.32:8080/api-core-servicios'


})

export const DiagnosticoAPI = axios.create({
    baseURL: 'http://localhost:8002/diagnostico'
})

export const InmunoAPI = axios.create({
    baseURL: ''
})

export const MacroMicroAPI = axios.create({
    baseURL: 'http://localhost:8007/macro-micro'
})

export const MaterialAPI = axios.create({
    baseURL: 'http://localhost:8008/material-examinado'
})

export const TincionesAPI = axios.create({
    baseURL: 'http://localhost:8080/tinciones'
})


