import axios from 'axios';
import { getSession } from 'next-auth/react';

export async function searchCodigosFonasa() {
    try {
        const { jwt } = await getSession();
        // const response = await axios.get(`http://10.69.206.32:8080/api-core-codigo-fonasa/api/fonasa`);
        const response = await axios.get(`/api/controllers/CodFonasa/getAllCodigos`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function saveCoreCodigoFonasa(data, id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.put(`/api/controllers/CodFonasa/update/${id}`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function newCoreCodigoFonasa(data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.post('/api/controllers/CodFonasa/newCodigo', data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function searchBiopsiaCodigoFonasa(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/ApaCodFonasa/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function saveBiopsiaCodigoFonasa(id, data, codigo) {
    try {
        const { jwt } = await getSession();
        const response = await axios.put(`/api/controllers/ApaCodFonasa/updateCodigo/${id}/${codigo}`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function postCodigoFonasaBiopsia(data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.post(`/api/controllers/ApaCodFonasa/postCodigo`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
