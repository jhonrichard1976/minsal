import axios from 'axios';
import { getSession } from 'next-auth/react';


export async function searchMacroMicro(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/MacroMicro/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function saveMacroMicros(data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.post(`/api/controllers/MacroMicro/postMacroMicro`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function updateMacroMicro(id, data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.patch(`/api/controllers/MacroMicro/update/${id}`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}



