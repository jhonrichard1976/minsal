import axios from 'axios';
import { getSession } from 'next-auth/react';

export async function searchTecnicas() {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/Tinciones/getAll`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function updateTecnica(id, data) {
    console.log(id, data);
    try {
        const { jwt } = await getSession();
        const response = await axios.patch(`/api/controllers/Tinciones/update/${id}`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function newTincion(data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.post(`/api/controllers/Tinciones/postTincion`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
