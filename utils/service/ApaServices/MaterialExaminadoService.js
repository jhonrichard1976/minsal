import axios from 'axios';
import { getSession } from 'next-auth/react';

export async function listMateriales() {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/Tinciones/getAll`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.error(error);
        return null;
    }
}

export async function saveMateriales(data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.post(`/api/controllers/Material
        /postMaterial`,data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function updateMateriales(data, id) {
    const { jwt } = await getSession();
    try {
        const response = await axios.post(`/api/controllers/Material
        /update/${id}`,data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function selectMaterial(idbiopsia) {
    const { jwt } = await getSession();
    try {
        const response = await axios.get(url + `/api/controllers/Material/idBiopsia/${idbiopsia}`,{
            headers: { Authorization: `Bearer ${jwt}` }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
