import axios from 'axios';
import { getSession } from 'next-auth/react';

export async function searchBiopsiaByid(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/Biopsia/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function searchMacroMicro(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/MacroMicro/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function searchDiagnosticosById(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/Diagnostico/biopsia/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        console.log(response.data);
        return response.data;
    } catch (err) {
        console.error(err);
        return null;
    }
}

export async function searchProfesionalById(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/CoreProfesionales/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function listProfesionales() {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/CoreProfesionales/getAll`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
