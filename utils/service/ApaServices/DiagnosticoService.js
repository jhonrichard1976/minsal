import axios from 'axios';
import { getSession } from 'next-auth/react';

export async function searchDiagnosticosByIdBiopsia(biopsiaId) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/Diagnostico/Biopsia/${biopsiaId}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (err) {
        console.error(err);
        return null;
    }
}

export async function saveDiagnostico(data) {
    try {
        const { jwt } = await getSession();
        const response = await axios.post(`/api/controllers/Diagnostico/postDiag`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function updateDiagnostico(data, id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.patch(`/api/controllers/Diagnostico/update/${id}`, data, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
