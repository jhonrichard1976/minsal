import axios from 'axios';
import { getSession } from 'next-auth/react';

export async function BiopsiaByFecha(desde, hasta) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/Biopsia/Fechas/${desde}/${hasta}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function CodFonasa(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/ApaCodFonasa/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function listServicios() {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/CoreServicios/getAll`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function servicioById(id) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/CoreServicios/id/${id}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function profesionalPorTipo(idTipoProfesional) {
    try {
        const { jwt } = await getSession();
        const response = await axios.get(`/api/controllers/CoreProfesionales/idTipoProfesional/${idTipoProfesional}`, {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        });   
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
