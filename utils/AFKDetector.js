import { useEffect, useState } from 'react';
import { useSession, signOut } from 'next-auth/react';
import { useEventListener } from 'primereact/hooks';
import { classNames } from 'primereact/utils';

const eventTypes = ['mousemove', 'mousedown', 'keypress', 'DOMMouseScroll', 'mousewheel', 'touchmove', 'MSPointerMove', 'touchstart', 'touchend', 'pointerdown', 'pointermove', 'pointerup', 'scroll'];

const AFKDetector = ({ timeout = 1 * 60 * 1000 }) =>  {
    const { data: session } = useSession();

  useEffect(() => {
    let timerId;
    bindKeyDown();
    bindKeyUp();

    // Reinicia el temporizador cuando el usuario interactúa con la página
    const resetTimer = () => {
      if (timerId) {
        clearTimeout(timerId);
      }
      timerId = setTimeout(() => {
        // Si el usuario está inactivo, cierra la sesión
        signOut();
      }, timeout);
    };

    // Agrega event listeners para detectar la actividad del usuario utilizando useEventListener
    const onKeyDown = (e) => {
        setPressed(true);

        if (e.code === 'Space') {
            //setValue('space');
            resetTimer();
            return;
        }

        setValue(e.key);
    };

    const [bindKeyDown, unbindKeyDown] = useEventListener({
        type: 'keydown',
        listener: (e) => {
            onKeyDown(e);
        }
    });

    const [bindKeyUp, unbindKeyUp] = useEventListener({
        type: 'keyup',
        listener: (e) => {
            setPressed(false);
        }
    });

    // Reinicia el temporizador al cargar la página
    resetTimer();

    // Elimina los event listeners al desmontar el componente
    return () => {
      clearTimeout(timerId);
      unbindKeyDown();
      unbindKeyUp();
    };
  }, [session]);
    const [pressed, setPressed] = useState(false);
    const [value, setValue] = useState('');

    const onKeyDown = (e) => {
        setPressed(true);

        if (e.code === 'Space') {
            setValue('space');

            return;
        }

        setValue(e.key);
    };

    const [bindKeyDown, unbindKeyDown] = useEventListener({
        type: 'keydown',
        listener: (e) => {
            onKeyDown(e);
        }
    });

    const [bindKeyUp, unbindKeyUp] = useEventListener({
        type: 'keyup',
        listener: (e) => {
            setPressed(false);
        }
    });
    useEffect(() => {
        bindKeyDown();
        bindKeyUp();

        return () => {
            unbindKeyDown();
            unbindKeyUp();
        };
    }, [bindKeyDown, bindKeyUp, unbindKeyDown, unbindKeyUp]);
}
export default AFKDetector
