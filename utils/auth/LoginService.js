import axios from 'axios';


export async function tokenizer(data){
    try {
        const response = await axios.post("http://ws.hcsba.cl/tokenizer/token", data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function validTokenizer(user, token){
    try {
        const response = await axios.get(`http://ws.hcsba.cl/logintokenizer/token/${user}/${token}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function getImagen(fileName){
    try {
        const response = await axios.get(`http://10.69.206.52:9029/perfiles/getfile/${fileName}`)
        return response.data
    } catch (error) {
        console.log(error);
        return null;

    }
}
export async function getPerfil(rut){
    try {
        const response = await axios.get(`http://10.69.206.52:9029/perfiles/${rut}`)
        return response.data
    } catch (error) {
        console.log(error);
        return null;


    }
}

