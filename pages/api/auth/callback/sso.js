


export default async function callback(req, res) {

  const user = req.query.user;
  const token = req.query.token;



  // Verificar si el usuario ya está autenticado
  if (user && token) {
    res.redirect(`/sso/callbacksso/${user}/${token}`);
    return;
  }

  try {

    res.redirect("/");
  } catch (error) {
    res.status(500).send(error.message);
  }
}
