// pages/api/image.js
import axios from 'axios';

export default async function handler(req, res) {
  const imageName = req.query.name;
  console.log(imageName);
  const imageUrl = `http://10.69.206.52:9029/uploads/${imageName}`;

  try {
    const response = await axios.get(imageUrl, { responseType: 'arraybuffer' });
    console.log(response);
    res.setHeader('Content-Type', response.headers['content-type']);
    return res.send(response.data);
  } catch (error) {
    res.status(500).json({ error: 'Error al cargar la imagen' });
  }
}
